<?php

/**
 * @file
 * Contains \Drupal\contextio_feeds_plugins\Feeds\Parser\Form\ContextIOParserConfigForm.
 */

namespace Drupal\contextio_feeds_plugins\Feeds\Parser\Form;

use Drupal\contextio_feeds_plugins\Feeds\Parser\ContextIOParser;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

/**
 * Configuration form for ContextIOParser.
 */
class ContextIOParserConfigForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();
    // Allowed derivative source operations.
    $derivative_ops = [
      ContextIOParser::DERIVATIVE_OP_ADD,
      ContextIOParser::DERIVATIVE_OP_EDIT,
      ContextIOParser::DERIVATIVE_OP_DELETE,
    ];
    // Get GET parameters.
    $derivative_id = \Drupal::request()->get('id');
    $derivative_op = \Drupal::request()->get('op');
    // Add a derivatives related configuration forms.
    if (!empty($derivative_op) && in_array($derivative_op, $derivative_ops)) {
      // Add new derivative source form.
      if ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) {
        $this->derivativeSourceForm($form, $derivative_op);
      }
      // Edit and delete derivative source forms.
      elseif (!empty($derivative_id) && array_key_exists($derivative_id, $config['derivative_sources'])) {
        // Delete operation.
        if ($derivative_op == ContextIOParser::DERIVATIVE_OP_DELETE) {
          // Check derivative dependents in case of delete operation.
          $dependents = $this->getDerivativeSourceDependents($config['derivative_sources'][$derivative_id]);
          // No dependents found, delete operation allowed.
          if (empty($dependents['source'])) {
            $this->derivativeSourceForm($form, $derivative_op, $config['derivative_sources'][$derivative_id]);
          }
          // Delete operation not allowed, list derivative source configs.
          else {
            $this->derivativeSources($form);
          }
        }
        // Edit operation.
        else {
          $this->derivativeSourceForm($form, $derivative_op, $config['derivative_sources'][$derivative_id]);
        }
      }
      else {
        $this->derivativeSources($form);
      }
    }
    // List derivative source configurations.
    else {
      $this->derivativeSources($form);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();
    $values = &$form_state->getValues();
    // Validate derivative source form. Required fields validated automatically.
    if (!empty($values['derivative_source']['op'])) {
      $derivative_op = $values['derivative_source']['op'];
      // Add or edit derivative.
      if (in_array($derivative_op, [ContextIOParser::DERIVATIVE_OP_ADD, ContextIOParser::DERIVATIVE_OP_EDIT])) {
        // The name of the field where replacement patterns supported.
        $sub_patterns_field_name = NULL;

        switch ($values['derivative_source']['type']) {
          case ContextIOParser::DERIVATIVE_TYPE_CONSTANT:
            $sub_patterns_field_name = 'value';
            if (empty($values['derivative_source']['value'])) {
              $form_state->setErrorByName('parser_configuration][derivative_source][value', $this->t('Value field is required.'));
            }
            break;

          case ContextIOParser::DERIVATIVE_TYPE_SIMPLE_REPLACE:
            $sub_patterns_field_name = 'replacement';
            if (empty($values['derivative_source']['source'])) {
              $form_state->setErrorByName('parser_configuration][derivative_source][source', $this->t('Source field is required.'));
            }
            if (empty($values['derivative_source']['search_string'])) {
              $form_state->setErrorByName('parser_configuration][derivative_source][search_string', $this->t('Search string field is required.'));
            }
            break;

          case ContextIOParser::DERIVATIVE_TYPE_REGEX_REPLACE:
            $sub_patterns_field_name = 'regex_replacement';
            if (empty($values['derivative_source']['source'])) {
              $form_state->setErrorByName('parser_configuration][derivative_source][source', $this->t('Source field is required.'));
            }
            if (empty($values['derivative_source']['regex_pattern'])) {
              $form_state->setErrorByName('parser_configuration][derivative_source][regex_pattern', $this->t('Regex pattern field is required.'));
            }
            break;
        }

        // Check replacement patterns related circle references.
        if (!empty($values['derivative_source']['name']) && !empty($values['derivative_source']['machine_name'])) {
          $matches = [];
          $mapping_sources = $this->plugin->getMappingSources();
          preg_match_all('/(\[source:([a-z0-9_]+)\])/', $values['derivative_source'][$sub_patterns_field_name], $matches);
          if (!empty($matches[2])) {
            $matches_unique = [];
            foreach ($matches[2] as $index => $source_machine_name) {
              // Check if replacement pattern is exist.
              if (!array_key_exists($source_machine_name, $mapping_sources)) {
                $form_state->setErrorByName('parser_configuration][derivative_source][' . $sub_patterns_field_name, $this->t('Unknown replacement pattern: @token.', [
                  '@token' => '[source:' . $source_machine_name . ']',
                ]));
              }
              // Check if derivative source reference is valid.
              elseif (($derivative_op == ContextIOParser::DERIVATIVE_OP_EDIT) && array_key_exists($source_machine_name, $config['derivative_sources'])) {
                if ($values['derivative_source']['num'] <= $config['derivative_sources'][$source_machine_name]['num']) {
                  $form_state->setErrorByName('parser_configuration][derivative_source][' . $sub_patterns_field_name, $this->t('Invalid derivative source reference: @token.', [
                    '@token' => '[source:' . $source_machine_name . ']',
                  ]));
                }
                elseif (!array_key_exists($source_machine_name, $matches_unique)) {
                  $matches_unique[$source_machine_name] = $matches[1][$index];
                }
              }
              // Ignore match duplications.
              elseif (!array_key_exists($source_machine_name, $matches_unique)) {
                $matches_unique[$source_machine_name] = $matches[1][$index];
              }
            }
            // Save replacement pattern dependencies.
            $values['derivative_source']['used_replacement_patterns'] = $matches_unique;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();
    $values = &$form_state->getValues();
    // Save derivative source related settings if any.
    if (!empty($values['derivative_source']['op'])) {
      switch ($values['derivative_source']['op']) {
        case ContextIOParser::DERIVATIVE_OP_ADD:
          // Get the largest derivative source num.
          $max_num = 0;
          foreach ($config['derivative_sources'] as $derivative_source) {
            if ($derivative_source['num'] > $max_num) {
              $max_num = $derivative_source['num'];
            }
          }
          // The new derivative source will get the largest num.
          $values['derivative_source']['num'] = ($max_num + 1);
          // Save new settings.
          $config['derivative_sources'][$values['derivative_source']['machine_name']] = $values['derivative_source'];
          drupal_set_message($this->t('Derivative source has been saved.'));
          break;

        case ContextIOParser::DERIVATIVE_OP_EDIT:
          // Update existing settings.
          $config['derivative_sources'][$values['derivative_source']['machine_name']] = $values['derivative_source'];
          drupal_set_message($this->t('Derivative source has been updated.'));
          break;

        case ContextIOParser::DERIVATIVE_OP_DELETE:
          // Delete settings.
          unset($config['derivative_sources'][$values['derivative_source']['original']['machine_name']]);
          drupal_set_message($this->t('Derivative source has been deleted.'));
          break;
      }

      // Sort derivative sources by their num after operations.
      uasort($config['derivative_sources'], ['ContextIOParserConfigForm', 'derivativeSourceNumCMP']);

      // Save config changes.
      // TODO: Save configuration after set?
      $this->plugin->setConfiguration($config);

      // TODO: feeds_cache_clear() in D7?

      // Redirect back.
      $form_state->setRedirectUrl(Url::fromUri(\Drupal::request()->getRequestUri()));
    }
  }

  /**
   * Constructs a derivative source related configuration form.
   *
   * @param array $form
   *   The basic config form the derivative source form will be added to.
   * @param string $derivative_op
   *   The operation type of the form. The following values are available:
   *   - ContextIOParser::DERIVATIVE_OP_ADD: Add derivative form.
   *   - ContextIOParser::DERIVATIVE_OP_EDIT: Edit derivative form.
   *   - ContextIOParser::DERIVATIVE_OP_DELETE: Delete derivative form.
   * @param array $derivative
   *   An array of derivative properties.
   */
  protected function derivativeSourceForm(array &$form, $derivative_op = ContextIOParser::DERIVATIVE_OP_ADD, array $derivative = []) {
    $config = $this->plugin->getConfiguration();

    $form['#tree'] = TRUE;
    // Add-edit derivative source form.
    if (in_array($derivative_op, [ContextIOParser::DERIVATIVE_OP_ADD, ContextIOParser::DERIVATIVE_OP_EDIT]) && !empty($derivative)) {
      // Construct an array of available mapping sources. Initialize with all
      // sources including derivatives.
      $available_sources = $this->plugin->getMappingSources();
      // Limit available sources for edited derivatives.
      if ($derivative_op == ContextIOParser::DERIVATIVE_OP_EDIT) {
        foreach ($config['derivative_sources'] as $key => $derivative_source) {
          // Skip the derivative source if its num is greater or equal than the
          // num of the currently edited derivative source.
          if ($derivative['num'] <= $derivative_source['num']) {
            unset($available_sources[$key]);
          }
        }
      }

      // Construct source options.
      $source_options = [];
      foreach ($available_sources as $key => $source) {
        $source_options[$key] = $source['name'] . ' (' . $key . ')';
      }

      $form['derivative_source'] = [
        '#type' => 'details',
        '#title' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? $this->t('Add derivative source') : $this->t('Edit derivative source'),
        '#open' => TRUE,
      ];
      // Store operation type for later usage.
      $form['derivative_source']['op'] = [
        '#type' => 'value',
        '#value' => $derivative_op,
      ];
      // Store original derivative settings for later usage.
      $form['derivative_source']['original'] = [
        '#type' => 'value',
        '#value' => $derivative,
      ];

      $form['derivative_source']['num'] = [
        '#type' => 'value',
        '#value' => empty($derivative['num']) ? 0 : $derivative['num'],
      ];
      $form['derivative_source']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['name'],
        '#description' => $this->t('The name of the derivative.'),
        '#required' => TRUE,
      ];
      $form['derivative_source']['machine_name'] = [
        '#type' => 'machine_name',
        '#machine_name' => [
          'source' => ['derivative_source', 'name'],
          'exists' => 'contextio_feeds_plugins_derivative_source_exists',
        ],
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['machine_name'],
        '#description' => $this->t('The machine-readable name the derivative.'),
        '#used_machine_names' => array_keys($this->plugin->getMappingSources()),
        '#disabled' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_EDIT),
      ];
      $form['derivative_source']['description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Description'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['description'],
        '#description' => $this->t('A short description of the derivative.'),
        '#maxlength' => 255,
      ];
      $form['derivative_source']['type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Type'),
        '#options' => [
          ContextIOParser::DERIVATIVE_TYPE_CONSTANT => $this->t('Constant value'),
          ContextIOParser::DERIVATIVE_TYPE_SIMPLE_REPLACE => $this->t('Simple string replace'),
          ContextIOParser::DERIVATIVE_TYPE_REGEX_REPLACE => $this->t('String replace with Regex'),
        ],
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? ContextIOParser::DERIVATIVE_TYPE_SIMPLE_REPLACE : $derivative['type'],
        '#required' => TRUE,
      ];

      // TODO: This element is required. Hint: form-element-label.html.twig.
      $form['derivative_source']['source'] = [
        '#type' => 'select',
        '#title' => $this->t('Source'),
        '#options' => $source_options,
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['source'],
        '#description' => $this->t('The source of the derivative. Derivative sources with lower numbers could be sources as well.'),
        '#states' => [
          'invisible' => [
            ':input[name="derivative_source[type]"]' => ['value' => ContextIOParser::DERIVATIVE_TYPE_CONSTANT],
          ],
        ],
      ];

      // Type: Constant value.
      // TODO: This element is required. Hint: form-element-label.html.twig.
      $form['derivative_source']['value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Value'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['value'],
        '#description' => $this->t("The value of the derivative. May contain replacement patterns."),
        '#states' => [
          'visible' => [
            ':input[name="derivative_source[type]"]' => ['value' => ContextIOParser::DERIVATIVE_TYPE_CONSTANT],
          ],
        ],
      ];
      // Type: Simple string replace.
      // TODO: This element is required. Hint: form-element-label.html.twig.
      $form['derivative_source']['search_string'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Search string'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['search_string'],
        '#description' => $this->t("The string value being searched for."),
        '#states' => [
          'visible' => [
            ':input[name="derivative_source[type]"]' => ['value' => ContextIOParser::DERIVATIVE_TYPE_SIMPLE_REPLACE],
          ],
        ],
      ];
      $form['derivative_source']['replacement'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Replacement'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['replacement'],
        '#description' => $this->t(
          "The replacement string that replaces all occurrences of the search string. Leave empty to remove matched strings.
           May contain replacement patterns."
        ),
        '#states' => [
          'visible' => [
            ':input[name="derivative_source[type]"]' => ['value' => ContextIOParser::DERIVATIVE_TYPE_SIMPLE_REPLACE],
          ],
        ],
      ];
      // Type: String replace / extraction using regular expressions.
      // TODO: This element is required. Hint: form-element-label.html.twig.
      $form['derivative_source']['regex_pattern'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Regex pattern'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['regex_pattern'],
        '#description' => $this->t(
          "The regex pattern being searched for. Parenthesis can be used for references in the replacement field.
          For example /$([0-9]+?)/ will match strings that begin with numbers and the numbers will be captured by
          the first ($1) parenthesis pair."
        ),
        '#states' => [
          'visible' => [
            ':input[name="derivative_source[type]"]' => ['value' => ContextIOParser::DERIVATIVE_TYPE_REGEX_REPLACE],
          ],
        ],
      ];
      $form['derivative_source']['regex_replacement'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Replacement'),
        '#default_value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? '' : $derivative['regex_replacement'],
        '#description' => $this->t(
          "The replacement string that replaces all occurrences of the regex pattern. Leave empty to remove matched patterns.
           May contain replacement patterns or regex pattern component references of the form \$n or \${n}. Every such reference
           will be replaced by the text captured by the n'th parenthesized pattern in the pattern field. n can be from 0 to 99,
           and \$0 or \${0} refers to the text matched by the whole pattern."
        ),
        '#states' => [
          'visible' => [
            ':input[name="derivative_source[type]"]' => ['value' => ContextIOParser::DERIVATIVE_TYPE_REGEX_REPLACE],
          ],
        ],
      ];

      // Used replacement patterns (see configFormValidate() for details).
      $form['derivative_source']['used_replacement_patterns'] = [
        '#type' => 'value',
        '#value' => ($derivative_op == ContextIOParser::DERIVATIVE_OP_ADD) ? [] : $derivative['used_replacement_patterns'],
      ];

      // Replacement patterns.
      if (!empty($available_sources)) {
        $form['derivative_source']['replacement_patterns'] = [
          '#type' => 'details',
          '#title' => $this->t('Replacement patterns'),
          '#open' => FALSE,
        ];
        $form['derivative_source']['replacement_patterns']['description'] = [
          '#prefix' => '<div class="description">',
          '#markup' => $this->t('Replacement patterns can be used to substitute other source values. Derivative sources with lower numbers can be also referenced.'),
          '#suffix' => '</div><br/>',
        ];

        $replacement_patterns = [];
        foreach ($available_sources as $key => $source) {
          $replacement_patterns[] = [
            Html::escape($source['name']),
            '[source:' . $key . ']',
            Html::escape($source['description']),
          ];
        }

        $form['derivative_source']['replacement_patterns']['table'] = [
          '#theme' => 'table',
          '#header' => [$this->t('Name'), $this->t('Token'), $this->t('Description')],
          '#rows' => $replacement_patterns,
          '#caption' => NULL,
          '#colgroups' => [],
          '#sticky' => FALSE,
          '#empty' => $this->t('There are no available replacement patterns.'),
        ];
      }
    }
    // Delete derivative source form.
    elseif (($derivative_op == ContextIOParser::DERIVATIVE_OP_DELETE) && !empty($derivative)) {
      $form['derivative_source'] = [
        '#type' => 'details',
        '#title' => $this->t('delete derivative source'),
        '#open' => TRUE,
      ];
      // Store operation type for later usage.
      $form['derivative_source']['op'] = [
        '#type' => 'value',
        '#value' => $derivative_op,
      ];
      // Store original derivative settings for later usage.
      $form['derivative_source']['original'] = [
        '#type' => 'value',
        '#value' => $derivative,
      ];
      $form['derivative_source']['message'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('Are you sure you want to delete the following derivative source?'),
        '#suffix' => '</p>',
      ];
      $form['derivative_source']['name'] = [
        '#prefix' => '<p>',
        '#markup' => Html::escape($derivative['name']) . ' (' . $derivative['machine_name'] . ')',
        '#suffix' => '</p>',
      ];
    }
  }

  /**
   * Helper function to list derivative source configs.
   *
   * @param array $form
   *   The basic config form the derivative config list will be added to.
   */
  protected function derivativeSources(array &$form) {
    $form['#tree'] = TRUE;
    $form['derivative_sources'] = [
      '#type' => 'details',
      '#title' => $this->t('Derivative sources'),
      '#open' => TRUE,
    ];
    $form['derivative_sources']['description'] = [
      '#prefix' => '<div class="description">',
      '#markup' => $this->t('Derivative sources will provide additional custom mapping sources for processor plugins.'),
      '#suffix' => '</div>',
    ];
    $form['derivative_sources']['action_links'] = [
      '#theme' => 'item_list',
      '#items' => [
        Link::fromTextAndUrl($this->t('Add derivative source'), Url::fromUri(\Drupal::request()->getRequestUri(), [
          'query' => ['op' => ContextIOParser::DERIVATIVE_OP_ADD],
        ]))->toString(),
      ],
      '#type' => 'ul',
      '#attributes' => [
        'class' => ['action-links'],
      ],
      '#prefix' => '<br/>',
      '#suffix' => '<br/>',
    ];

    if (!empty($config['derivative_sources'])) {
      $form['derivative_sources']['table'] = [
        '#theme' => 'table',
        '#header' => [
          [
            'data' => $this->t('Num'),
            'width' => '1%',
          ],
          [
            'data' => $this->t('Name'),
            'width' => '33%',
          ],
          $this->t('Type'),
          $this->t('Settings'),
          [
            'data' => t('Operations'),
            'width' => '1%',
            'colspan' => 2,
          ],
        ],
        '#attributes' => [
          'id' => 'derivative-sources-table',
        ],
        '#attached' => [
          'library' => ['contextio_feeds_plugins/contextio_parser.config_form'],
        ],
        '#rows' => [],
        '#caption' => NULL,
        '#colgroups' => [],
        '#sticky' => FALSE,
        '#empty' => NULL,
      ];

      // Get mapping sources with derivatives.
      $mapping_sources = $this->plugin->getMappingSources();

      $derivative_num = 1;
      // List derivative source configuration settings.
      foreach ($config['derivative_sources'] as $source) {
        // Construct a delete link for the derivative.
        $delete_link = '<div style="color: #666;">' . $this->t('delete') . '</div>';

        // Check dependents.
        $dependents = $this->getDerivativeSourceDependents($source, TRUE);
        // Override delete link if no dependents found.
        if (empty($dependents['source'])) {
          $delete_link = Link::fromTextAndUrl($this->t('delete'), Url::fromUri(\Drupal::request()->getRequestUri(), [
            'query' => [
              'op' => ContextIOParser::DERIVATIVE_OP_DELETE,
              'id' => $source['machine_name'],
            ],
          ]))->toString();
        }

        // Construct a table row.
        $table_row = [
          'num' => [
            'data' => [
              '#prefix' => '<div style="text-align: center;">',
              '#markup' => '#' . $derivative_num++,
              '#suffix' => '</div>',
            ],
          ],
          'name' => [
            'data' => [
              'name' => [
                '#prefix' => '<div class="name collapsed">',
                '#markup' => Html::escape($source['name']) . ' (' . $source['machine_name'] . ')',
                '#suffix' => '</div>',
              ],
            ],
            'class' => ['name'],
          ],
          'type' => [
            'data' => [],
          ],
          'settings' => [
            'data' => [],
            'class' => ['settings'],
          ],
          'edit' => [
            'data' => [
              '#markup' => Link::fromTextAndUrl($this->t('edit'), Url::fromUri(\Drupal::request()->getRequestUri(), [
                'query' => [
                  'op' => ContextIOParser::DERIVATIVE_OP_EDIT,
                  'id' => $source['machine_name'],
                ],
              ]))->toString(),
            ],
          ],
          'delete' => [
            'data' => [
              '#markup' => $delete_link,
            ],
          ],
        ];

        // TODO: This below code should be placed into a template instead.
        // Display description and dependents.
        $description = '<div class="description-and-dependents">';
        // Description.
        if (!empty($source['description'])) {
          $description .= '<div class="description">' . Html::escape($source['description']) . '</div>';
        }
        // Dependents.
        if (!empty($dependents)) {
          $description .= '<div class="dependents">';
          // Required by.
          if (!empty($dependents['source'])) {
            $dependent_sources = [];
            foreach ($dependents['source'] as $dependent) {
              $num = '#' . $dependent['num'];
              $dependent_sources[] = [
                '#markup' => $num . ' ' . Html::escape($dependent['name']) . ' (' . $dependent['machine_name'] . ')',
              ];
            }
            $description .= '<div class="required-by"><div class="label">' . $this->t('Required by:') . '</div>';
            $dependent_sources_list = [
              '#theme' => 'item_list',
              '#title' => NULL,
              '#items' => $dependent_sources,
              '#type' => 'ul',
            ];
            $description .= \Drupal::service('renderer')->render($dependent_sources_list);
            $description .= '</div>';
          }
          // Used by.
          if (!empty($dependents['replacement_pattern'])) {
            $dependent_replacement_patterns = array();
            foreach ($dependents['replacement_pattern'] as $dependent) {
              $num = '#' . $dependent['num'];
              $dependent_replacement_patterns[] = [
                '#markup' => $num . ' ' . Html::escape($dependent['name']) . ' (' . $dependent['machine_name'] . ')',
              ];
            }
            $description .= '<div class="used-by"><div class="label">' . $this->t('Used by:') . '</div>';
            $dependent_replacement_patterns_list = [
              '#theme' => 'item_list',
              '#title' => NULL,
              '#items' => $dependent_replacement_patterns,
              '#type' => 'ul',
            ];
            $description .= \Drupal::service('renderer')->render($dependent_replacement_patterns_list);
            $description .= '</div>';
          }
          $description .= '</div>';
        }
        else {
          $description .= '<div class="dependents">' . $this->t('Used by: None') . '</div>';
        }
        $description .= '</div>';

        $table_row['name']['data']['description']['#markup'] = $description;

        // Display additional properties based on source type.
        switch ($source['type']) {
          case ContextIOParser::DERIVATIVE_TYPE_CONSTANT:
            $table_row['type']['data'] = [
              '#markup' => $this->t('Constant value'),
            ];
            // Mark replacement patterns "disabled" if no related derivatives
            // found (they have been deleted).
            $value = $source['value'];
            foreach ($source['used_replacement_patterns'] as $derivative_machine_name => $pattern) {
              if (!array_key_exists($derivative_machine_name, $mapping_sources)) {
                $value = str_replace($pattern, '<span style="color: #666;"><s>' . $pattern . '</s></span>', $value);
              }
            }
            $table_row['settings']['data']['value'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Value:</strong> @value', ['@value' => $value]),
              '#suffix' => '</div>',
            ];
            break;

          case ContextIOParser::DERIVATIVE_TYPE_SIMPLE_REPLACE:
            $table_row['type']['data'] = [
              '#markup' => $this->t('Simple string replace'),
            ];
            $table_row['settings']['data']['source'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Source:</strong> @source', [
                '@source' => $mapping_sources[$source['source']]['name'] . ' (' . $source['source'] . ')',
              ]),
              '#suffix' => '</div>',
            ];
            $table_row['settings']['data']['search_string'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Search string:</strong> @search_string', ['@search_string' => $source['search_string']]),
              '#suffix' => '</div>',
            ];
            // Mark replacement patterns "disabled" if no related derivatives
            // found (they have been deleted).
            $replacement = $source['replacement'];
            foreach ($source['used_replacement_patterns'] as $derivative_machine_name => $pattern) {
              if (!array_key_exists($derivative_machine_name, $mapping_sources)) {
                $replacement = str_replace($pattern, '<span style="color: #666;"><s>' . $pattern . '</s></span>', $replacement);
              }
            }
            $table_row['settings']['data']['replacement'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Replacement:</strong> @replacement', ['@replacement' => $replacement]),
              '#suffix' => '</div>',
            ];
            break;

          case ContextIOParser::DERIVATIVE_TYPE_REGEX_REPLACE:
            $table_row['type']['data'] = [
              '#markup' => $this->t('String replace with Regex'),
            ];
            $table_row['settings']['data']['source'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Source:</strong> @source', [
                '@source' => $mapping_sources[$source['source']]['name'] . ' (' . $source['source'] . ')',
              ]),
              '#suffix' => '</div>',
            ];
            $table_row['settings']['data']['regex_pattern'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Regex pattern:</strong> @regex_pattern', ['@regex_pattern' => $source['regex_pattern']]),
              '#suffix' => '</div>',
            ];
            // Mark replacement patterns "disabled" if no related derivatives
            // found (they have been deleted).
            $regex_replacement = $source['regex_replacement'];
            foreach ($source['used_replacement_patterns'] as $derivative_machine_name => $pattern) {
              if (!array_key_exists($derivative_machine_name, $mapping_sources)) {
                $regex_replacement = str_replace($pattern, '<span style="color: #666;"><s>' . $pattern . '</s></span>', $regex_replacement);
              }
            }
            $table_row['settings']['data']['regex_replacement'] = [
              '#prefix' => '<div class="setting">',
              '#markup' => $this->t('<strong>Replacement:</strong> @regex_replacement', ['@regex_replacement' => $regex_replacement]),
              '#suffix' => '</div>',
            ];
            break;
        }

        // Add table row.
        $form['derivative_sources']['table']['#rows'][] = $table_row;
      }
    }
    else {
      $form['derivative_sources']['table'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('No derivative sources have been added yet.'),
        '#suffix' => '</p>',
      ];
    }
  }

  /**
   * Compares the num of two derivative sources.
   *
   * This function is used in uasort PHP function calls.
   *
   * @param array $a
   *   Property array of the first derivative source.
   * @param array $b
   *   Property array of the second derivative source.
   *
   * @return int
   *   An integer value according to the comparison.
   *
   * @see configFormSubmit()
   */
  protected static function derivativeSourceNumCMP($a, $b) {
    if ($a['num'] == $b['num']) {
      return 0;
    }
    return ($a['num'] < $b['num']) ? -1 : 1;
  }

  /**
   * Helper function to get derivative source dependents.
   *
   * @param string $derivative_source
   *   Property array of the derivative source.
   * @param bool $check_replacement_patterns
   *   Boolean indicating that replacement patterns should be checked or not
   *   during the dependent search.
   *
   * @return array
   *   An associative array of dependents keyed by dependent type.
   */
  protected function getDerivativeSourceDependents($derivative_source, $check_replacement_patterns = FALSE) {
    $config = $this->plugin->getConfiguration();
    
    $dependents = [];
    $source_num = 1;
    foreach ($config['derivative_sources'] as $source) {
      // Skip the derivative itself and derivatives with lower num.
      if ($source['num'] <= $derivative_source['num']) {
        $source_num++;
        continue;
      }
      // Check source dependents.
      if ($source['source'] == $derivative_source['machine_name']) {
        $dependents['source'][] = [
          'name' => $source['name'],
          'machine_name' => $source['machine_name'],
          'num' => $source_num,
        ];
      }
      // Check replacement_pattern dependents.
      if ($check_replacement_patterns && array_key_exists($derivative_source['machine_name'], $source['used_replacement_patterns'])) {
        $dependents['replacement_pattern'][] = [
          'name' => $source['name'],
          'machine_name' => $source['machine_name'],
          'num' => $source_num,
        ];
      }
      $source_num++;
    }
    return $dependents;
  }

}
