<?php

/**
 * @file
 * Contains \Drupal\contextio_feeds_plugins\Feeds\Parser\ContextIOMessageParser.
 */

namespace Drupal\contextio_feeds_plugins\Feeds\Parser;

use Drupal\feeds\FeedInterface;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;

/**
 * Defines a Context.IO message fetcher.
 *
 * @FeedsParser(
 *   id = "contextio_message",
 *   title = @Translation("Context.IO Message Parser"),
 *   description = @Translation("Parse Context.IO specific email message data.")
 * )
 */
class ContextIOMessageParser extends ContextIOParser {

  /**
   * {@inheritdoc}
   */
  public function contextIOParse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {
    $fetcher = $this->feedType->getFetcher();
    // Check fetcher's type and message.
    if (get_class($fetcher) == 'ContextIOMessageFetcher') {
      $messages = $fetcher_result->getMessages();
      if (!empty($messages)) {
        // Init results.
        $results = [];
        // Process messages.
        foreach ($messages as $message) {
          // Result defaults.
          $result = [
            'id' => $message['message_id'],
            'date' => $message['date'],
            'subject' => $message['subject'],
            'from_email' => empty($message['addresses']['from']['email']) ? '' : $message['addresses']['from']['email'],
            'from_name' => empty($message['addresses']['from']['name']) ? '' : $message['addresses']['from']['name'],
            'to_emails' => '',
            'flags' => '',
            'body_plain' => '',
            'body_html' => '',
          ];
          // Make a list from "To" email addresses.
          if (!empty($message['addresses']['to'])) {
            $to_count = count($message['addresses']['to']) - 1;
            foreach ($message['addresses']['to'] as $index => $to) {
              $result['to_emails'] .= $to['email'];
              if ($index < $to_count) {
                $result['to_emails'] .= ', ';
              }
            }
          }
          // Make a list from flags.
          if (!empty($message['flags'])) {
            $result['flags'] = implode(', ', $message['flags']);
          }
          // Check message bodies. For now plain text and HTML type supported.
          if (!empty($message['body'])) {
            foreach ($message['body'] as $body) {
              switch ($body['type']) {
                case 'text/plain':
                  $result['body_plain'] = $body['content'];
                  break;

                case 'text/html':
                  $result['body_html'] = $body['content'];
                  break;
              }
            }
          }
          // Add result.
          $results[] = $result;
        }
        return new ParserResult($results);
      }
    }
    // Unknown fetcher, return with empty result.
    return new ParserResult([]);
  }

  /**
   * {@inheritdoc}
   */
  public function contextIOGetMappingSources() {
    $sources = [];
    // Load the fetcher object.
    $fetcher = $this->feedType->getFetcher();
    // Check fetcher's type.
    if (get_class($fetcher) == 'ContextIOMessageFetcher') {
      $fetcher_config = $fetcher->getConfiguration();
      // Default sources.
      $sources += [
        'id' => [
          'name' => $this->t('Id'),
          'description' => $this->t('The unique and persistent id assigned by Context.IO to the message.'),
        ],
        'date' => [
          'name' => $this->t('Date'),
          'description' => $this->t('The Unix timestamp the message sent from the origin.'),
        ],
        'subject' => [
          'name' => $this->t('Subject'),
          'description' => $this->t('The subject of the email message.'),
        ],
        'from_email' => [
          'name' => $this->t('From - Email'),
          'description' => $this->t('The email address the message received from.'),
        ],
        'from_name' => [
          'name' => $this->t('From - Name'),
          'description' => $this->t('The account name the message received from (not always available).'),
        ],
        'to_emails' => [
          'name' => $this->t('To emails'),
          'description' => $this->t('A comma separated email address list the message sent to (not always available).'),
        ],
      ];
      // Optionally add bodies related sources.
      if (!empty($fetcher_config['additional_options']['include_body'])) {
        $sources['body_plain'] = [
          'name' => $this->t('Body - Plain text'),
          'description' => $this->t('The body of the email message in plain text format.'),
        ];
        $sources['body_html'] = [
          'name' => $this->t('Body - HTML'),
          'description' => $this->t('The body of the email message in HTML format.'),
        ];
      }
      // Optionally add a flags related source.
      if (!empty($fetcher_config['additional_options']['include_flags'])) {
        $sources['flags'] = [
          'name' => $this->t('Flags'),
          'description' => $this->t('A comma separated IMAP flag list the message marked with.'),
        ];
      }
    }
    return $sources;
  }

}
