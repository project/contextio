<?php

/**
 * @file
 * Contains \Drupal\contextio_feeds_plugins\Feeds\Fetcher\ContextIOMessageFetcher.
 */

namespace Drupal\contextio_feeds_plugins\Feeds\Fetcher;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ClearableInterface;
use Drupal\feeds\Plugin\Type\FeedPluginFormInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\StateInterface;
use Drupal\Component\Utility\Html;
use Drupal\contextio_feeds_plugins\Feeds\Fetcher\Result\ContextIOMessageFetcherResult;

/**
 * Defines a Context.IO message fetcher.
 *
 * @FeedsFetcher(
 *   id = "contextio_message",
 *   title = @Translation("Context.IO Message Fetcher"),
 *   description = @Translation("Download email messages from Context.IO."),
 *   configuration_form = "Drupal\contextio_feeds_plugins\Feeds\Fetcher\Form\ContextIOMessageFetcherConfigForm"
 * )
 */
class ContextIOMessageFetcher extends PluginBase implements ClearableInterface, FeedPluginFormInterface, FetcherInterface {

  const IMPORT_TYPE_NUM = 1;
  const IMPORT_TYPE_ALL = 2;

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $config = $this->getConfiguration();
    $feed_config = $feed->getConfigurationFor($this);
    $state = $feed->getState(StateInterface::FETCH);
    // Initialize batch settings. Add a batch_ prefix to own batch related
    // variable names to avoid name conflicts with Feeds's own variables.
    if (!isset($state->batch_progress)) {
      $batch_size = $config['additional_options']['batch_size'];
      if ($feed_config['number_of_messages'] < $batch_size) {
        $batch_size = $feed_config['number_of_messages'];
      }
      $state->batch_progress = 0;
      $state->batch_messages = $this->getMessages($batch_size, $feed_config['offset']);
      $state->batch_offset = $feed_config['offset'] + count($state->batch_messages);
      if ($feed_config['import_type'] == self::IMPORT_TYPE_NUM) {
        $state->batch_total = $feed_config['number_of_messages'];
      }
      else {
        try {
          $account = contextio_get_account($config['mailbox']['account_id']);
          if (!empty($account['nb_messages'])) {
            $state->batch_total = $account['nb_messages'];
          }
          else {
            \Drupal::logger('contextio_feeds_plugins')->error('Missing nb_messages property after a contextio_get_account() call.');
          }
        }
        catch (Exception $e) {
          \Drupal::logger('contextio_feeds_plugins')->error('An error occurred during a contextio_get_account() call. Error message: @message', [
            '@message' => $e->getMessage(),
          ]);
        }
      }
    }

    // Try to download the next message batch.
    if (empty($state->batch_messages) && ($state->batch_progress < $state->batch_total)) {
      $batch_size = $config['additional_options']['batch_size'];
      if (($state->batch_total - $state->batch_progress) < $batch_size) {
        $batch_size = $state->batch_total - $state->batch_progress;
      }
      $state->batch_messages = $this->getMessages($batch_size, $state->batch_offset);
      $state->batch_offset += count($state->batch_messages);
    }

    // Process messages.
    if (!empty($state->batch_messages)) {
      $messages = $state->batch_messages;
      $state->batch_messages = [];
      $state->batch_progress += count($messages);
      $state->progress($state->batch_total, $state->batch_progress);
      return new ContextIOMessageFetcherResult($messages);
    }

    // The number of the messages that can be imported is lower than the number
    // we wanted to fetch, so override the state progress and finish the import.
    $state->progress($state->batch_progress, $state->batch_progress);
    return new ContextIOMessageFetcherResult([]);
  }

  /**
   * Helper function to fetch messages.
   *
   * @param int $limit
   *   The maximum number of messages to download.
   * @param int $offset
   *   The offset the download process will start at.
   *
   * @return array
   *   An array of messages.
   */
  protected function getMessages($limit, $offset) {
    $config = $this->getConfiguration();

    $filters = [];
    // Add filter options.
    foreach ($config['filter_options'] as $key => $value) {
      if (!empty($value)) {
        $filters[$key] = $value;
      }
    }

    // Limit can be lower then the default batch size.
    $filters['limit'] = $limit;
    // Additional options.
    $filters['include_body'] = (int) $config['additional_options']['include_body'];
    $filters['include_flags'] = (int) $config['additional_options']['include_flags'];
    // Source options.
    $filters['offset'] = $offset;

    $messages = [];
    try {
      $messages_data = contextio_get_messages($config['mailbox']['account_id'], $filters);
      if (!empty($messages_data)) {
        foreach ($messages_data as $message) {
          $messages[] = $message;
        }
      }
    }
    catch (Exception $e) {
      \Drupal::logger('contextio_feeds_plugins')->error('An error occurred during a contextio_get_messages() call. Error message: @message', [
        '@message' => $e->getMessage(),
      ]);
    }

    return $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function clear(FeedInterface $feed, StateInterface $state) {
    // TODO: Implement clear() method.
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mailbox' => [
        'account_id' => '',
        'email_address' => '',
        'connect_token' => '',
      ],
      'additional_options' => [
        'batch_size' => 50,
        'include_body' => 0,
        'include_flags' => 0,
      ],
      'filter_options' => [
        'subject' => '',
        'to' => '',
        'from' => '',
        'cc' => '',
        'bcc' => '',
        'folder' => '',
        'date_before' => '',
        'date_after' => '',
      ],
    ];
  }

  /**
   * {@inheritodc}
   */
  public function sourceDefaults() {
    return [
      'number_of_messages' => 100,
      'import_type' => self::IMPORT_TYPE_NUM,
      'offset' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildFeedForm(array $form, FormStateInterface $form_state, FeedInterface $feed) {
    $config = $this->getConfiguration();
    $feed_config = $feed->getConfigurationFor($this);

    $form['#tree'] = TRUE;
    $form['email_address'] = [
      '#type' => 'item',
      '#title' => $this->t('Email address'),
      '#markup' => Html::escape($config['mailbox']['email_address']),
    ];
    $form['import_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Import type'),
      '#options' => [
        self::IMPORT_TYPE_NUM => $this->t('Import a specified number of messages'),
        self::IMPORT_TYPE_ALL => $this->t('Import all messages'),
      ],
      '#default_value' => $feed_config['import_type'],
    ];
    $form['number_of_messages'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of messages to import'),
      '#default_value' => $feed_config['number_of_messages'],
      '#size' => 7,
      '#states' => [
        'visible' => [
          ':input[name="plugin[fetcher][import_type]"]' => [
            'value' => self::IMPORT_TYPE_NUM,
          ],
        ],
      ],
    ];
    $form['offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Offset'),
      '#default_value' => $feed_config['offset'],
      '#description' => $this->t('Start the import process at this offset (zero-based).'),
      '#size' => 7,
    ];

    $filter_options_titles = [
      'subject' => $this->t('Subject'),
      'to' => $this->t('To'),
      'from'=> $this->t('From'),
      'cc' => $this->t('CC'),
      'bcc' => $this->t('BCC'),
      'folder' => $this->t('Folder'),
      'date_before' => $this->t('Date before'),
      'date_after' => $this->t('Date after'),
    ];

    $filter_settings = [];
    // Collect non-empty filter settings.
    foreach ($config['filter_options'] as $key => $value) {
      if (!empty($value)) {
        if (in_array($key, ['subject', 'to', 'from', 'cc', 'bcc', 'folder'])) {
          $filter_settings[] = [
            '#markup' => '<strong>' . $filter_options_titles[$key] . ':</strong> ' . Html::escape($value),
          ];
        }
        elseif (in_array($key, ['date_before', 'date_after'])) {
          $filter_settings[] = [
            '#markup' => '<strong>' . $filter_options_titles[$key] . ':</strong> ' . date('Y-m-d H:i', $value),
          ];
        }
      }
    }

    // Show non-empty filter settings.
    if (!empty($filter_settings)) {
      $form['filter_settings'] = [
        '#type' => 'details',
        '#title' => $this->t('Filter settings'),
        '#open' => FALSE,
      ];
      $form['filter_settings']['message'] = [
        '#prefix' => '<div class="description">',
        '#markup' => $this->t('Filter settings are treated as an AND combination.'),
        '#suffix' => '</div>',
      ];
      $form['filter_settings']['list'] = [
        '#prefix' => '<p>',
        '#theme' => 'item_list',
        '#items' => $filter_settings,
        '#type' => 'ul',
        '#suffix' => '</p>',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateFeedForm(array &$form, FormStateInterface $form_state, FeedInterface $feed) {
    $values = $form_state->getValues();
    if ($values['import_type'] == self::IMPORT_TYPE_NUM) {
      $number_of_messages = $values['number_of_messages'];
      if (!(is_numeric($number_of_messages) && ((int) $number_of_messages >= 1))) {
        $field_name = 'plugin][fetcher][number_of_messages';
        $form_state->setErrorByName($field_name, $this->t('Number of messages to import must be an integer value and greater than or equal to 1.'));
      }
    }

    $offset = $values['offset'];
    if (!(is_numeric($offset) && ((int) $offset >= 0))) {
      $field_name = 'plugin][fetcher][offset';
      $form_state->setErrorByName($field_name, $this->t('Offset must be an integer value and greater than or equal to 0.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFeedForm(array &$form, FormStateInterface $form_state, FeedInterface $feed) {
    $values = &$form_state->getValues();
    // Convert string values to integers.
    if ($values['import_type'] == self::IMPORT_TYPE_NUM) {
      $number_of_messages = &$values['number_of_messages'];
      $number_of_messages = (int) $number_of_messages;
    }

    $offset = &$values['offset'];
    $offset = (int) $offset;

    // Save values.
    parent::submitFeedForm($form, $form_state, $feed);
  }

}
