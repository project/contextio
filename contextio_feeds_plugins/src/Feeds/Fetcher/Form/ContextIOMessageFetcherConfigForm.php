<?php

/**
 * @file
 * Contains \Drupal\contextio_feeds_plugins\Feeds\Fetcher\Form\ContextIOMessageFetcherConfigForm.
 */

namespace Drupal\contextio_feeds_plugins\Feeds\Fetcher\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

/**
 * Configuration form for ContextIOMessageFetcher.
 */
class ContextIOMessageFetcherConfigForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();

    $form['#tree'] = TRUE;
    
    $key = \Drupal::state()->get('contextio_key', NULL);
    $secret = \Drupal::state()->get('contextio_secret', NULL);

    if (empty($key) || empty($secret)) {
      $form['set_up_key_and_secret'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t("To be able to import email messages from Context.IO at first @set_up_link your customer key and secret.", [
          '@set_up_link' => Link::fromTextAndUrl($this->t('set up'), Url::fromRoute('contextio_admin.admin', [], [
            'attributes' => [
              'target' => '_blank',
            ],
          ]))->toString(),
        ]),
        '#suffix' => '</p>',
      ];
      return $form;
    }

    // Default value of the email address depends on a valid connect token.
    $email_address_default_value = $config['mailbox']['email_address'];

    $received_connect_token = \Drupal::request()->get('contextio_token');
    // Ignore FeedsConfigurable::hasConfigForm() with a $form_state check.
    if (!empty($form_state) && !empty($received_connect_token)) {
      // Avoid connect token recheck if has been already used.
      if (empty($config['mailbox']['connect_token']) || ($config['mailbox']['connect_token'] != $received_connect_token)) {
        try {
          // Get the connect token related account id and update settings.
          $connect_token = contextio_get_connect_token('', $received_connect_token);
          if (!empty($connect_token['account']['id'])) {
            $config_tmp = $this->plugin->getConfiguration();
            $config_tmp['mailbox']['account_id'] = $connect_token['account']['id'];
            $config_tmp['mailbox']['connect_token'] = $received_connect_token;
            // TODO: Save configuration after set?
            $this->plugin->setConfiguration($config_tmp);
            drupal_get_messages('status');
            drupal_set_message($this->t('Your settings have been saved. The new mailbox has been added to your Context.IO account.'));
          }
          else {
            $email_address_default_value = '';
            drupal_set_message($this->t('The new mailbox creation was not successful. Please try again.'), 'error');
            \Drupal::logger('contextio_feeds_plugins')->error('Missing account][id property after contextio_get_connect_token() call.');
          }
        }
        catch (Exception $e) {
          $email_address_default_value = '';
          drupal_set_message($this->t('The new mailbox creation was not successful. Please try again.'), 'error');
          \Drupal::logger('contextio_feeds_plugins')->error('An error occurred during a contextio_get_connect_token() call. Error message: @message', [
            '@message' => $e->getMessage(),
          ]);
        }
      }
    }

    // Add new mailbox warning message container.
    $form['add_new_mailbox_message'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'add-new-mailbox-message',
        'style' => 'display: none;',
      ],
    ];
    // We set a message but right after that we will clear the message queue.
    drupal_set_message($this->t(
      'After settings have been saved you will be redirected to grant access to Context.IO to 
       use your email address. Once the process is complete you will be redirected back to this page.'
    ), 'warning');
    $form['add_new_mailbox_message']['message'] = [
      '#theme' => 'status_messages',
      '#message_list' => drupal_get_messages('warning'),
      '#status_headings' => [
        'status' => $this->t('Status message'),
        'error' => $this->t('Error message'),
        'warning' => $this->t('Warning message'),
      ],
    ];
    $form['#attached'] = [
      'library' => ['contextio_feeds_plugins/contextio_message_fetcher.config_form'],
    ];

    // Mailbox settings.
    $form['mailbox'] = [
      '#type' => 'details',
      '#title' => $this->t('Mailbox'),
      '#open' => TRUE,
    ];
    $form['mailbox']['account_id'] = [
      '#type' => 'value',
      '#value' => $config['mailbox']['account_id'],
    ];
    $form['mailbox']['email_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address'),
      '#description' => $this->t("The email address messages will be imported from. You can read about Context.IO's End User Opt Out @here.", [
        '@here' => Link::fromTextAndUrl($this->t('here'), Url::fromUri('https://optout.context.io/', [
          'attributes' => [
            'target' => '_blank',
          ],
        ]))->toString(),
      ]),
      '#default_value' => $email_address_default_value,
      '#required' => TRUE,
    ];
    $form['mailbox']['add_new_mailbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add new mailbox'),
      '#description' => $this->t(
        "If checked a new mailbox will be created with the provided email address in your Context.IO account. Use this option if
         you haven't set up your email address before."
      ),
      // We always want to reset this field value, so we don't save the value.
      '#default_value' => 0,
    ];
    $form['mailbox']['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First name'),
      '#description' => $this->t('The first name associated to the new mailbox.'),
      // We always want to reset this field value, so we don't save the value.
      '#default_value' => '',
      '#states' => [
        'visible' => [
          ':input[name="fetcher_configuration[mailbox][add_new_mailbox]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mailbox']['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#description' => $this->t('The last name associated to the new mailbox.'),
      // We always want to reset this field value, so we don't save the value.
      '#default_value' => '',
      '#states' => [
        'visible' => [
          ':input[name="fetcher_configuration[mailbox][add_new_mailbox]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Additional options.
    $form['additional_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Additional options'),
      '#open' => TRUE,
    ];
    $form['additional_options']['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#default_value' => $config['additional_options']['batch_size'],
      '#description' => $this->t('The maximum number of messages to fetch per request. The maximum limit is 100.'),
      '#size' => 5,
    ];
    $form['additional_options']['include_body'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import message bodies'),
      '#description' => $this->t(
        "If checked message bodies will be imported. Since message bodies must be retrieved from the IMAP server,
         expect a performance hit when setting this parameter."
      ),
      '#default_value' => $config['additional_options']['include_body'],
    ];
    $form['additional_options']['include_flags'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import message flags'),
      '#description' => $this->t(
        "If checked message flags will be imported. Since message flags must be retrieved from the IMAP server,
         expect a performance hit when setting this parameter."
      ),
      '#default_value' => $config['additional_options']['include_flags'],
    ];

    // Filter options settings.
    $form['filter_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter options'),
      '#open' => FALSE,
    ];
    $form['filter_options']['description'] = [
      '#prefix' => '<div class="description">',
      '#markup' => $this->t(
        "<p>Each of the <strong>To</strong>, <strong>From</strong>, <strong>CC</strong> and <strong>BCC</strong>
         filter options can be set to a comma-separated list of email addresses. These multiple addresses are treated
         as an OR combination.</p><p>You can set more than one filter option at once. Multiple filter options are
         treated as an AND combination.</p>"
      ),
      '#suffix' => '</div>',
    ];
    $form['filter_options']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config['filter_options']['subject'],
      '#description' => $this->t("Get messages whose subject matches this search string. Regular expressions are allowed between / characters."),
    ];
    $form['filter_options']['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#default_value' => $config['filter_options']['to'],
      '#description' => $this->t('Email address(es) of messages have been sent to.'),
    ];
    $form['filter_options']['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From'),
      '#default_value' => $config['filter_options']['from'],
      '#description' => $this->t('Email address(es) of messages have been received from.'),
    ];
    $form['filter_options']['cc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CC'),
      '#default_value' => $config['filter_options']['cc'],
      '#description' => $this->t("Email address(es) of a contact CC'ed on the messages."),
    ];
    $form['filter_options']['bcc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BCC'),
      '#default_value' => $config['filter_options']['bcc'],
      '#description' => $this->t("Email address(es) of a contact BCC'ed on the messages."),
    ];
    $form['filter_options']['folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Folder'),
      '#default_value' => $config['filter_options']['folder'],
      '#description' => $this->t(
        "Filter messages by the folder (or Gmail label). This parameter can be the complete folder name with the appropriate
         hierarchy delimiter for the mail server being queried (eg. Inbox/My folder) or the \"symbolic name\" of the folder
         (eg. \\Starred). The symbolic name refers to attributes used to refer to special use folders in a language-independent
         way. See @rfc_link.", [
        '@rfc_link' => Link::fromTextAndUrl($this->t('RFC-6154'), Url::fromUri('http://tools.ietf.org/html/rfc6154'))->toString(),
      ]),
    ];
    $date_before_empty = empty($config['filter_options']['date_before']);
    $form['filter_options']['date_before'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date before'),
      '#default_value' => $date_before_empty ? '' : date('Y-m-d H:i', $config['filter_options']['date_before']),
      '#size' => 15,
      '#description' => $this->t('Only include messages after a given timestamp (eg. @date).', [
        '@date' => date('Y-m-d H:i'),
      ]),
    ];
    $date_after_empty = empty($config['filter_options']['date_after']);
    $form['filter_options']['date_after'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date after'),
      '#default_value' => $date_after_empty ? '' : date('Y-m-d H:i', $config['filter_options']['date_after']),
      '#size' => 15,
      '#description' => $this->t('Only include messages before a given timestamp (eg. @date).', [
        '@date' => date('Y-m-d H:i'),
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = &$form_state->getValues();
    if (!empty($values['mailbox'])) {
      if (!empty($values['mailbox']['email_address'])) {
        if (!empty($values['mailbox']['add_new_mailbox'])) {
          try {
            $params = [
              'callback_url' => Url::fromUri(\Drupal::request()->getRequestUri(), ['absolute' => TRUE]),
              'email' => $values['mailbox']['email_address'],
              'first_name' => $values['mailbox']['first_name'],
              'last_name' => $values['mailbox']['last_name'],
            ];
            $connect_token = contextio_add_connect_token(NULL, $params);
            if (!empty($connect_token['success']) && !empty($connect_token['browser_redirect_url'])) {
              // Save browser_redirect_url into values for later usage.
              $values['mailbox']['browser_redirect_url'] = $connect_token['browser_redirect_url'];
            }
            else {
              $form_state->setErrorByName('fetcher_configuration][mailbox][email_address', $this->t('An error occurred during the connect token creation. Please try again.'));
              \Drupal::logger('contextio_feeds_plugins')->error('Missing or FALSE success property after a contextio_add_connect_token() call.');
            }
          }
          catch (Exception $e) {
            $form_state->setErrorByName('fetcher_configuration][mailbox][email_address', $this->t('An error occurred during the connect token creation. Message: @message', [
              '@message' => $e->getMessage(),
            ]));
            \Drupal::logger('contextio_feeds_plugins')->error('An error occurred during a contextio_add_connect_token() call. Error message: @message', [
              '@message' => $e->getMessage(),
            ]);
          }
        }
        else {
          try {
            // Try to get account details and store its id for later usage.
            $accounts = contextio_accounts_list(['email' => $values['mailbox']['email_address']]);
            if (!empty($accounts[0]['id'])) {
              $values['mailbox']['account_id'] = $accounts[0]['id'];
            }
            else {
              $form_state->setErrorByName('fetcher_configuration][mailbox][email_address', $this->t('No account holder found for the provided mailbox email address. Please try again.'));
              \Drupal::logger('contextio_feeds_plugins')->error('Missing account][0][id property after a contextio_accounts_list() call.');
            }
          }
          catch (Exception $e) {
            $form_state->setErrorByName('fetcher_configuration][mailbox][email_address', $this->t('An error occurred while trying to get the Context.IO accounts list. Message: @message', [
              '@message' => $e->getMessage(),
            ]));
            \Drupal::logger('contextio_feeds_plugins')->error('An error occurred during a contextio_accounts_list() call. Error message: @message', [
              '@message' => $e->getMessage(),
            ]);
          }
        }
      }

      $batch_size = $values['additional_options']['batch_size'];
      if (!(is_numeric($batch_size) && (((int) $batch_size >= 1) && ((int) $batch_size <= 100)))) {
        $form_state->setErrorByName('fetcher_configuration][additional_options][batch_size', $this->t('Batch size must be an integer value between 1 and 100.'));
      }

      // Check date field values' format.
      $field_names = ['date_before', 'date_after'];
      foreach ($field_names as $field_name) {
        if (!empty($values['filter_options'][$field_name])) {
          $date = DateTime::createFromFormat('Y-m-d H:i', $values['filter_options'][$field_name]);
          if (!($date && $date->format('Y-m-d H:i') == $values['filter_options'][$field_name])) {
            $form_state->setErrorByName('filter_options][' . $field_name, $this->t('The allowed date format is YY-MM-DD hh:mm'));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = &$form_state->getValues();
    if (!empty($values['mailbox'])) {
      // Convert date values to timestamps before saving.
      $field_names = ['date_before', 'date_after'];
      foreach ($field_names as $field_name) {
        if (!empty($values['filter_options'][$field_name])) {
          $timestamp = strtotime($values['filter_options'][$field_name]);
          $values['filter_options'][$field_name] = $timestamp;
        }
      }
      // Convert limit and offset values to integers.
      $values['additional_options']['batch_size'] = (int) $values['additional_options']['batch_size'];

      parent::submitConfigurationForm($form, $form_state);

      // Redirect the user to the new mailbox setup.
      if (!empty($values['mailbox']['browser_redirect_url'])) {
        // We use drupal_goto() since there is no option here to use the more
        // appropriate $form_state['redirect'].
        $form_state->setRedirectUrl(Url::fromUri($values['mailbox']['browser_redirect_url']));
      }
    }
  }

}
