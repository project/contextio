<?php

/**
 * @file
 * Contains \Drupal\contextio_feeds_plugins\Feeds\Fetcher\Result\ContextIOMessageFetcherResult.
 */

namespace Drupal\contextio_feeds_plugins\Feeds\Fetcher\Result;

use Drupal\feeds\Result\FetcherResult;

/**
 * ContextIOMessageFetcher specific result class.
 */
class ContextIOMessageFetcherResult extends FetcherResult {

  /**
   * An array of messages to return.
   *
   * @var array
   */
  protected $messages;

  /**
   * Constructs a ContextIOMessageFetcherResult object.
   *
   * @param array $messages
   *   Property array of messages.
   */
  public function __construct(array $messages) {
    parent::__construct('');
    $this->messages = $messages;
  }

  /**
   * Get messages.
   */
  public function getMessages() {
    return $this->messages;
  }

}
