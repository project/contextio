/**
 * @file
 * Scripts to show/hide OAuth secret field's content.
 */

(function ($) {
  /**
   * Reveal OAuth secret behavior.
   */
  Drupal.behaviors.contextIOAdminRevealOAuthSecret = {
    attach: function (context, settings) {
      $('#contextio-reveal-secret').click(function() {
        var $contextIOSecret = $('#contextio-secret');
        if ($(this).is(':checked')) {
          $contextIOSecret.prop('type', 'text');
        } else {
          $contextIOSecret.prop('type', 'password');
        }
      });
    }
  }
})(jQuery);
