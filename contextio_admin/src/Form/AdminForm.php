<?php

/**
 * @file
 * Contains \Drupal\contextio_admin\Form\AdminForm.
 */

namespace Drupal\contextio_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class AdminForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contextio_admin_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('OAuth consumer key and secret'),
      '#open' => TRUE,
    ];
    $form['credentials']['contextio_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#required' => TRUE,
      '#default_value' => contextio_decrypt(\Drupal::state()->get('contextio_key', '')),
    ];
    $form['credentials']['contextio_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Secret'),
      '#required' => TRUE,
      '#attributes' => [
        'id' => 'contextio-secret',
        'value' => contextio_decrypt(\Drupal::state()->get('contextio_secret', '')),
      ],
      '#field_suffix' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Reveal'),
        '#attributes' => [
          'id' => 'contextio-reveal-secret',
        ],
        '#attached' => [
          'library' => ['contextio_admin/reveal-oauth-secret'],
        ],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save credentials'),
    ];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete credentials'),
      '#delete_credentials' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Don't validate form when deleting credentials.
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#delete_credentials'])) {
      return;
    }

    // Test credentials.
    try {
      $contextio = contextio_get_object($form_state->getValue('contextio_key'), $form_state->getValue('contextio_secret'));
      $token = $contextio->listConnectTokens();
      if ($token === FALSE) {
        $form_state->setErrorByName('contextio_key', $this->t("Couldn't connect to Context.IO."));
        $form_state->setErrorByName('contextio_secret', "");
      }
    }
    catch (Exception $e) {
      // Nothing here, the empty key/secret fields are handled with Drupal's
      // validator.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#delete_credentials'])) {
      // Delete credentials.
      \Drupal::state()->delete('contextio_key');
      \Drupal::state()->delete('contextio_secret');

      drupal_set_message($this->t('Credentials have been successfully deleted!'));
    }
    else {
      // Save credentials.
      \Drupal::state()->set('contextio_key', contextio_encrypt($form_state->getValue('contextio_key')));
      \Drupal::state()->set('contextio_secret', contextio_encrypt($form_state->getValue('contextio_secret')));

      drupal_set_message($this->t('Settings have been successfully saved!'));
    }
  }
}
